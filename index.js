console.log('Hello World');

let firstName = 'John';
console.log('First Name: ' + firstName);

let lastName = 'Smith';
console.log('Last Name: ' + lastName);

let age = 30;
console.log('Age: ' + age);


function printUserInfo(firstName, lastName, age){
	
	let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
	console.log("Hobbies: ");
	console.log(hobbies);


	let workAddress = {
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska"
	};
	console.log("Work Address: ");
	console.log(workAddress);


	console.log(firstName + ' ' + lastName + ' is ' + age + ' years of age.');

	console.log("This was printed inside of the function");

	console.log(hobbies);
	console.log("This was printed inside of the function");

	console.log(workAddress);

};

printUserInfo('John','Smith', 30);


function returnFunction(isMarried){
	return isMarried;

};

//console.log(returnFunction(true));

let value = returnFunction(true);

console.log("The value of isMarried is: " + value);
 





